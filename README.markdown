JC Tierney Tendril Code Assignment
=============================

This repository houses the code for the assigment defined below:

>Print the following descriptive statistics of a list of numbers: mean, median, range, and standard deviation. The numbers will be provided in a text file, one per line. Your program should accept the name of the text file as a command-line argument. Code defensively. 

Building
-------

This project is built using the Apache Buildr tool. It will import all dependencies, execute all tests and build an executable JAR in the target directory.


Executing
--------

The JAR will contain not only all custom code, but any necessary imported code to be able to run using only the JAR.

    java -jar target/tendril-assignment-0.1.0.jar /path/to/valid/file.txt

Output
------

Below is a sample output.

    Statistical Summary
    Mean: 5.000000
    Median: 50.000000
    Range: 7.000000
    Standard Deviation: 2.138090