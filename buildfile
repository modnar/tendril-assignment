repositories.remote << 'http://mirrors.ibiblio.org/maven2/'

COMPILEDEPS = 'org.apache.commons:commons-math3:jar:3.2',
              'commons-io:commons-io:jar:2.4',
              'org.apache.commons:commons-lang3:jar:3.1'

TESTDEPS = 'junit:junit:jar:4.4',
		   'org.mockito:mockito-core:jar:1.8.3',
           'org.objenesis:objenesis:jar:1.0'

define 'tendril-assignment' do
  project.version = '0.1.0'
  compile.with COMPILEDEPS
  test.with TESTDEPS
  package(:jar).with(:manifest => _('src/main/resources/MANIFEST.MF')).merge(COMPILEDEPS)
end