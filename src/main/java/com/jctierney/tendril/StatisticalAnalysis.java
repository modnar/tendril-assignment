package com.jctierney.tendril;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;

public class StatisticalAnalysis {
    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            throw new FileNotFoundException("No file parameter provided");
        }

        File file = new File(args[0]);
        if (!file.exists()) {
            throw new FileNotFoundException(String.format("Invalid file name provided: %s", file.getAbsolutePath()));
        }

        DataProcessor dataProcessor = new DataProcessor(FileUtils.lineIterator(file));
        System.out.println(dataProcessor.getSummary());
    }
}


