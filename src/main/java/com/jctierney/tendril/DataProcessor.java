package com.jctierney.tendril;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;
import org.apache.commons.math3.stat.descriptive.rank.Max;
import org.apache.commons.math3.stat.descriptive.rank.Median;
import org.apache.commons.math3.stat.descriptive.rank.Min;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DataProcessor {
    private DescriptiveStatistics statistics;

    public DataProcessor(Iterator<String> iterator) {
        List<Double> values = new ArrayList<Double>();
        while (iterator.hasNext()) {
            values.add(Double.valueOf(iterator.next()));
        }
        statistics = new DescriptiveStatistics(ArrayUtils.toPrimitive(values.toArray(new Double[values.size()])));
    }

    public String getSummary() {
        return String.format("Statistical Summary\n" +
                "Mean: %f\n" +
                "Median: %f\n" +
                "Range: %f\n" +
                "Standard Deviation: %f\n",
                getMean(),
                getMedian(),
                getRange(),
                getStandardDeviation());
    }

    public double getMean() {
        return statistics.getMean();
    }

    public double getMedian() {
        return statistics.getPercentile(50);
    }

    public double getRange() {
        return statistics.getMax() - statistics.getMin();
    }

    public double getStandardDeviation() {
        return statistics.getStandardDeviation();
    }
}
