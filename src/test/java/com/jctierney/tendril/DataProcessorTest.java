package com.jctierney.tendril;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.*;

public class DataProcessorTest {

    @Test
    public void shouldProvideValidValuesForOneInputs() {
        DataProcessor dp = new DataProcessor(Arrays.asList("1").iterator());
        compareDoubles(1.0, dp.getMean());
        compareDoubles(1.0, dp.getMedian());
        compareDoubles(0.0, dp.getRange());
        compareDoubles(0.0, dp.getStandardDeviation());
    }

    @Test
    public void shouldProvideValidValuesForTwoInputs() {
        DataProcessor dp = new DataProcessor(Arrays.asList("0","2").iterator());
        compareDoubles(1.0, dp.getMean());
        compareDoubles(1.0, dp.getMedian());
        compareDoubles(2.0, dp.getRange());
        compareDoubles(1.4142135623730951, dp.getStandardDeviation());
    }

    @Test
    public void shouldProvideValidValuesForThreeInputs() {
        DataProcessor dp = new DataProcessor(Arrays.asList("0","1","2").iterator());
        compareDoubles(1.0, dp.getMean());
        compareDoubles(1.0, dp.getMedian());
        compareDoubles(2, dp.getRange());
        compareDoubles(1.0, dp.getStandardDeviation());
    }

    @Test
    public void shouldProvideValidValuesForZeroInputs() {
        DataProcessor dp = new DataProcessor(new ArrayList<String>().iterator());
        compareDoubles(Double.NaN, dp.getMean());
        compareDoubles(Double.NaN, dp.getMedian());
        compareDoubles(Double.NaN, dp.getRange());
        compareDoubles(Double.NaN, dp.getStandardDeviation());
    }

    @Test
    public void summaryShouldMatch() {
        DataProcessor dp = new DataProcessor(Arrays.asList("1").iterator());
        assertEquals("Statistical Summary\n" +
                "Mean: 1.000000\n" +
                "Median: 1.000000\n" +
                "Range: 0.000000\n" +
                "Standard Deviation: 0.000000\n", dp.getSummary());
    }

    private void compareDoubles(double one, double two) {
        assertEquals(String.valueOf(one), String.valueOf(two));
    }
}
