package com.jctierney.tendril;

import org.junit.Test;

import java.io.FileNotFoundException;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class StatisticalAnalysisTest {
    @Test
    public void noArgumentsShouldFail() throws Exception {
        try {
            StatisticalAnalysis.main(new String[0]);
            fail("Should throw FileNotFoundException");
        } catch (FileNotFoundException e) {
            //expected result
        }
    }

    @Test
    public void twoArgumentsShouldFail() throws Exception {
        try {
            StatisticalAnalysis.main(new String[2]);
            fail("Should throw FileNotFoundException");
        } catch (FileNotFoundException e) {
            //expected result
        }
    }

    @Test
    public void invalidFileShouldFail() throws Exception {
        try {
            StatisticalAnalysis.main(new String[]{"my/bad/file/path"});
            fail("Should throw FileNotFoundException");
        } catch (FileNotFoundException e) {
            assertTrue(e.getMessage().contains("my/bad/file/path"));
        }
    }

    @Test
    public void fileWithInvalidNumberShouldFail() throws Exception {
        try {
            StatisticalAnalysis.main(new String[]{"src/test/resources/invalid_number.txt"});
            fail("Should throw NumberFormatException");
        } catch (NumberFormatException e) {
            //expected result
        }
    }

    @Test
    public void emptyFileShouldSucceed() throws Exception {
        StatisticalAnalysis.main(new String[]{"src/test/resources/empty.txt"});
    }

    @Test
    public void validFileShouldSucceed() throws Exception {
        StatisticalAnalysis.main(new String[]{"src/test/resources/valid.txt"});
    }
}
